class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.string :titulo
      t.text :comentario
      t.date :data

      t.timestamps
    end
  end
end
