class CreateLojas < ActiveRecord::Migration[5.0]
  def change
    create_table :lojas do |t|
    	t.string :nome
    	t.text :descricao
    	t.string :email

      t.timestamps null: false
    end
  end
end
