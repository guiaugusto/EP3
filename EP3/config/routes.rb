Rails.application.routes.draw do
 
   resources :homes
  devise_for :users

  resources :lojas do
  	resources :reviews
  end 	
  # root 'lojas#index'

  root 'homes#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
