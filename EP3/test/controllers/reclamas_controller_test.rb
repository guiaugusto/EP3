require 'test_helper'

class ReclamasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @reclama = reclamas(:one)
  end

  test "should get index" do
    get reclamas_url
    assert_response :success
  end

  test "should get new" do
    get new_reclama_url
    assert_response :success
  end

  test "should create reclama" do
    assert_difference('Reclama.count') do
      post reclamas_url, params: { reclama: { comentario: @reclama.comentario, data: @reclama.data, titulo: @reclama.titulo } }
    end

    assert_redirected_to reclama_url(Reclama.last)
  end

  test "should show reclama" do
    get reclama_url(@reclama)
    assert_response :success
  end

  test "should get edit" do
    get edit_reclama_url(@reclama)
    assert_response :success
  end

  test "should update reclama" do
    patch reclama_url(@reclama), params: { reclama: { comentario: @reclama.comentario, data: @reclama.data, titulo: @reclama.titulo } }
    assert_redirected_to reclama_url(@reclama)
  end

  test "should destroy reclama" do
    assert_difference('Reclama.count', -1) do
      delete reclama_url(@reclama)
    end

    assert_redirected_to reclamas_url
  end
end
